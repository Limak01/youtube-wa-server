FROM alpine:latest

WORKDIR /usr/src/proxy

COPY package*.json ./
COPY proxy.js ./
COPY data ./data/
COPY notifications ./notifications/
COPY utils ./utils/

RUN apk update 
RUN apk add nodejs
RUN apk add npm

RUN npm install

EXPOSE 8000
CMD ["node", "proxy.js"]
