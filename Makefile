clean:
	rm -rf /opt/youtube-wa

install: clean
	mkdir /opt/youtube-wa
	cp -rf utils /opt/youtube-wa/utils
	cp -rf data /opt/youtube-wa//data
	cp -rf notifications /opt/youtube-wa/notifications
	cp package.json /opt/youtube-wa/
	cp proxy.js /opt/youtube-wa/
	cp youtube-wa.service /etc/systemd/system/
	cd /opt/youtube-wa && npm install
uninstall:
	rm -rf /opt/youtube-wa

