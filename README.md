Server for [youtube-wa](https://gitlab.com/Limak01/youtube-wa). This server is needed for youtube-wa to properly work.

# Installation

Download source code or git clone:
     
     # Installation
     git clone https://gitlab.com/Limak01/youtube-wa-server.git
     cd youtube-wa-server
     sudo make install

     # Enable and start youtube-wa service
     sudo systemctl enable youtube-wa.service
     sudo systemctl start youtube-wa.service

     # Removal
     sudo make uninstall

