const notifier = require("node-notifier")

function showNotification(title, message) {
    notifier.notify(
        {
            title: title,
            message: message,
        }
    )
}

module.exports = showNotification
