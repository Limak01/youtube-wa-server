#! /usr/bin/env node

const express = require("express")
const CronJob = require("cron").CronJob
const {getSubscriptions, setSubscriptions} = require("./utils/subscriptions")
const {fetchFeed, fetchSingleFeed} = require("./utils/fetchData")
const {getFeed} = require("./utils/feed")
const PORT = process.env.PORT || 49160

const feedJob = new CronJob('0 */5 * * * *', fetchFeed)

feedJob.start()

const app = express()
app.use(express.json())

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader("Access-Control-Allow-Headers", "Origin, Content-Type")
    next()
}) 

app.get("/feed", async (req, res) => {
    const feed = getFeed()
    
    res.json(feed)
})

app.post("/subscribe", (req, res) => {
    const subscriptions = getSubscriptions()
    subscriptions.push(req.body)
    
    setSubscriptions(subscriptions)
    fetchSingleFeed(req.body)
})

app.post("/unsubscribe", (req, res) => {
    const subscriptions = getSubscriptions()
    const channel = req.body
    const filteredSubs = subscriptions.filter(item => item.id !== channel.id)

    setSubscriptions(filteredSubs)

    fetchFeed()
})

app.get("/subscriptions", (req, res) => {
    const subs = getSubscriptions()
    res.json(subs)
})

app.listen(PORT)
