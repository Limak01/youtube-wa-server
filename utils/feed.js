const fs = require("fs")

function getFeed() {
    const feed = fs.readFileSync("data/feed.json")

    return JSON.parse(feed)
}

function setFeed(feed) {
    const data = JSON.stringify(feed)
    
    fs.writeFileSync("data/feed.json", data)
}

module.exports  = {getFeed, setFeed}


