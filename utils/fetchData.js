const {getSubscriptions} = require("./subscriptions")
const {getFeed, setFeed} = require("./feed")
const axios = require("axios")
const parseString = require("xml2js").parseString
const showNotification = require("../notifications/notifications")

function olderThan2Weeks(date) {
    const currentDate = new Date()
    const checkDate = new Date(date)

    const diffInDays = Math.floor((currentDate - checkDate) / 1000 / 60 / 60 / 24)
    
    if(diffInDays > 14) return true

    return false
}

async function fetchFeed() {
    const subscriptions = getSubscriptions()
    const feed = getFeed()
    const url = `https://www.youtube.com/feeds/videos.xml?channel_id=`

    if(subscriptions.length != 0) {
         try {
             const videos = []

             for(let sub of subscriptions) {
                 const response = await axios.get(url + sub.id)

                 parseString(response.data, (err, result) => {
                     if(err) console.log("Error parsing xml document")
         
                     const entries = result.feed.entry
                     
                     for(let entry of entries) {
                         const video = {
                             thumbnail: entry["media:group"][0]["media:thumbnail"][0].$.url,
                             title: entry.title[0],
                             author: entry.author[0].name[0],
                             pubDate: entry.published[0],
                             link: entry.link[0].$.href
                         }
                         
                         if(olderThan2Weeks(video.pubDate)) continue

                         videos.push(video)
                     }
                 })
             }

             videos.sort((a, b) => {
                 const date1 = new Date(a.pubDate)
                 const date2 = new Date(b.pubDate)
                 
                 return date2 - date1
             })

             if(feed.length != 0) {
                 if(feed[0].title != videos[0].title)
                     showNotification(videos[0].author, videos[0].title)
             }
             
             setFeed(videos)
         } catch(error) {
             console.log(error)
         }
    } else setFeed([]) 
}

async function fetchSingleFeed(channel) {
    const url = `https://www.youtube.com/feeds/videos.xml?channel_id=`
    let feed = getFeed()
    const videos = []

    try {
        const response = await axios.get(url + channel.id)

        parseString(response.data, (err, result) => {
            if(err) console.log("Error parsing xml document")

            const entries = result.feed.entry
            
            for(let entry of entries) {
                const video = {
                    thumbnail: entry["media:group"][0]["media:thumbnail"][0].$.url,
                    title: entry.title[0],
                    author: entry.author[0].name[0],
                    pubDate: entry.published[0],
                    link: entry.link[0].$.href
                }
                
                videos.push(video)
            }
        })  
        
        feed = feed.concat(videos)

        feed.sort((a, b) => {
            const date1 = new Date(a.pubDate)
            const date2 = new Date(b.pubDate)
            
            return date2 - date1
        })

        setFeed(feed)
    } catch(err) {
        console.log(err)
    }
}

module.exports = {fetchFeed, fetchSingleFeed}
