const fs = require("fs")

function setSubscriptions(subscriptions) {
    const data = JSON.stringify(subscriptions)
    fs.writeFileSync("data/subscriptions.json", data)
}


function getSubscriptions() {
    const subscriptions = fs.readFileSync("data/subscriptions.json")

    return JSON.parse(subscriptions)
}

module.exports = {setSubscriptions, getSubscriptions}
